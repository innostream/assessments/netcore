import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  accountID = `Es16JpIi7ewY1vdB`;

  constructor() {}

  deposit = async (amount: number): Promise<number> => {
    const rawResponse = await fetch('http://localhost:5170/api/v1/deposit', {
      method: 'POST',
      body: JSON.stringify({ amount }),
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json',
        Accept: 'application/json',
        'X-Account-ID': this.accountID,
      },
      redirect: 'error',
    });

    if (rawResponse.ok) {
      const content = await rawResponse.json();
      return content.balance;
    } else {
      throw new Error('Deposit failed');
    }
  };
}
