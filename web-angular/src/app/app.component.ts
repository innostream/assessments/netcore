import { Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ApiService } from '../services/api.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, FormsModule, CommonModule],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss',
})
export class AppComponent {
  amount: number = 500;
  balance?: number;

  constructor(private apiService: ApiService) {}

  async performDeposit() {
    try {
      this.balance = await this.apiService.deposit(this.amount);
    } catch (err) {
      this.balance = undefined;
      alert(err);
    }
  }
}
