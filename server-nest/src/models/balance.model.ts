import { Column, DataType, Model, Table, PrimaryKey } from 'sequelize-typescript'

@Table
export class Balance extends Model {
  @PrimaryKey
  @Column
  account_id: string

  @Column(DataType.DOUBLE)
  balance: number
}
