import { Controller, Post, Body, Req } from '@nestjs/common'
import { AppService } from './app.service'
import { DepositToAccountRequest, DepositToAccountResponse } from './dto/deposit-to-account.dto'
import { LockService } from './lock.service'

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private readonly lockService: LockService,
  ) {}

  @Post('api/v1/deposit')
  async deposit(
    @Req() request: Request,
    @Body() body: DepositToAccountRequest,
  ): Promise<DepositToAccountResponse> {
    // Retrieve account id from middleware
    const account = request['auth_account'] as string

    // One at a time! Prevent simultaneous access to the same account.
    return this.lockService.lock(account, async () => {
      const balance = await this.appService.deposit(account, body.amount)

      // Respond to caller with balance, JSON output by convention
      return { balance }
    })
  }
}
