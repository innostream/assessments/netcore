import { Injectable } from '@nestjs/common'
import { InjectModel } from '@nestjs/sequelize'
import { Balance } from './models/balance.model'

@Injectable()
export class AppService {
  constructor(
    @InjectModel(Balance)
    private balanceModel: typeof Balance,
  ) {}

  async deposit(account: string, amount: number): Promise<number> {
    // Retrieve existing balance (if any)
    let record = await this.balanceModel.findByPk(account)

    if (!record) {
      // Create their balance if missing
      record = await this.balanceModel.create({ account_id: account, balance: 0 })
    }

    // Increment
    record.balance += amount
    record.save()

    return record.balance
  }
}
