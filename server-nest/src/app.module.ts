import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common'
import { SequelizeModule } from '@nestjs/sequelize'
import { AppController } from './app.controller'
import { AppService } from './app.service'
import { Balance } from './models/balance.model'
import { LockService } from './lock.service'
import { AccountMiddleware } from './account.middleware'

@Module({
  imports: [
    // Configure Sequelize to use an in-memory SQLite database
    SequelizeModule.forRoot({
      dialect: 'sqlite',
      storage: ':memory:',
      autoLoadModels: true,
      synchronize: true,
      models: [Balance],
    }),
    SequelizeModule.forFeature([Balance]),
  ],
  controllers: [AppController], // Routes
  providers: [LockService, AppService], // Lock service (string keyed mutex) and app service (business logic)
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AccountMiddleware).forRoutes(AppController) // Account middleware
  }
}
