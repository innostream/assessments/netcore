import { Injectable } from '@nestjs/common'

@Injectable()
export class LockService {
  private locks: Map<string, Promise<void>> = new Map()

  async lock<T>(key: string, fn: () => Promise<T>): Promise<T> {
    while (this.locks.has(key)) {
      // Wait for the existing lock to resolve
      await this.locks.get(key)
    }

    let resolve: () => void
    const lock = new Promise<void>((res) => (resolve = res))
    this.locks.set(key, lock)

    try {
      // Execute the provided function while the lock is active
      return await fn()
    } finally {
      // Release the lock
      this.locks.delete(key)
      resolve!() // Resolve the lock promise
    }
  }
}
