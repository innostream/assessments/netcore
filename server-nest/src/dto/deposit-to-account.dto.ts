export class DepositToAccountRequest {
  amount: number
}
export class DepositToAccountResponse {
  balance: number
}
