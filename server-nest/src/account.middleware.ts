import { Injectable, NestMiddleware } from '@nestjs/common'
import { Request, Response, NextFunction } from 'express'

@Injectable()
export class AccountMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    // Retrieve header
    const account = req.headers['x-account-id']

    if (!account) {
      // Reject request if header missing
      res.status(400).contentType('text/plain').send('Missing X-Account-ID header')
    } else {
      // Otherwise place it into the "state bag" that is shared with the endpoint
      req['auth_account'] = account
      next()
    }
  }
}
