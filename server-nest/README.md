# Getting Started

This assessment project uses a Node NestJS app. To start:

- Run `npm i`
- Run `npm run start`

For the assessment, the server is expected to be running and listening on port 5170. This is already
set in the default main.ts listen call.
