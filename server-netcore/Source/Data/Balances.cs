namespace server.Data;

public class Balances
{
    public string AccountId { get; set; } = String.Empty;

    public decimal Balance { get; set; }
}
