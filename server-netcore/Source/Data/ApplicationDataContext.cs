using Microsoft.EntityFrameworkCore;

namespace server.Data;

public class ApplicationDataContext : DbContext
{
    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseSqlite("Data Source=server.sqlite");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<Balances>(entity =>
        {
            entity.HasKey(e => e.AccountId);
            entity.Property(e => e.Balance).HasColumnType("decimal(18, 2)");
        });

        base.OnModelCreating(modelBuilder);
    }

    public DbSet<Balances> Balances { get; set; }
}
