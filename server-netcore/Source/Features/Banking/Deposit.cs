using FastEndpoints;
using server.Data;
using System.Text.Json.Serialization;

namespace server.Features.Banking;

public class Deposit
{
    // Request DTO for depositing money into the customer account.
    public class Request
    {
        [JsonPropertyName("amount")]
        public decimal Amount { get; set; }
    }

    // Deposit response DTO. Returns updated balance.
    public class Response
    {
        [JsonPropertyName("balance")]
        public decimal Balance { get; set; }
    }

    public class Handler : Endpoint<Request, Response>
    {
        private ApplicationDataContext Context { get; set; }
        public Handler(ApplicationDataContext db)
        {
            Context = db;
        }

        public override void Configure()
        {
            Post("/api/v1/deposit");
            AllowAnonymous();
        }

        public override async Task HandleAsync(Request r, CancellationToken c)
        {
            // Retrieve account id from middleware
            var state = ProcessorState<Middleware.AccountStateBag>();

            decimal accountBalance = 0;

            // One at a time! Prevent simultaneous access to the same account.
            lock (String.Intern(state.AccountId))
            {
                // Retrieve existing balance (if any)
                var balance = Context.Balances.Find(state.AccountId);
                if (balance == null)
                {
                    // Create their balance if missing
                    balance = new Balances() { AccountId = state.AccountId };
                    Context.Balances.Add(balance);
                }

                // Increment
                balance.Balance += r.Amount;
                Context.SaveChanges();

                accountBalance = balance.Balance;
            }

            // Respond to caller with balance, JSON output by convention
            await SendAsync(new Response { Balance = accountBalance });
        }
    }
}
