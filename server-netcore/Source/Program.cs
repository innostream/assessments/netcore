using FastEndpoints;
using Microsoft.EntityFrameworkCore;

namespace server
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Register a database context factory, used by singletons / long running services to create + dispose of connections
            builder.Services.AddDbContextFactory<Data.ApplicationDataContext>();
            // Register database context for normal transient usage
            builder.Services.AddDbContext<Data.ApplicationDataContext>();

            // Allow access from any origin
            builder.Services.AddCors(options =>
            {
                options.AddPolicy(name: "wide-open",
                                  policy =>
                                  {
                                      policy.AllowAnyHeader().AllowAnyMethod().AllowAnyOrigin();
                                  });
            });

            // Register FastEndpoints
            builder.Services
               .AddFastEndpoints();

            // Prepare app
            var app = builder.Build();

            // Create a transient scope and retrieve the database context, then apply any pending migrations
            using (var scope = app.Services.CreateScope())
            {
                var db = scope.ServiceProvider.GetRequiredService<Data.ApplicationDataContext>();
                db.Database.Migrate();
            }

            // Set CORS policy
            app.UseCors("wide-open");

            // Configure FastEndpoints
            app.UseFastEndpoints(c =>
            {
                c.Endpoints.Configurator = ep =>
                {
                    // Add middleware as "fake auth", to ensure the account id is present in the request
                    ep.PreProcessor<Middleware.Account>(Order.Before);
                };
            });

            // Startup the app
            app.Run();
        }
    }
}