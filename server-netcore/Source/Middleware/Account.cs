﻿using FastEndpoints;

namespace server.Middleware;

public class AccountStateBag
{
    public string AccountId { get; set; } = String.Empty;
}

public class Account : GlobalPreProcessor<AccountStateBag>
{

    public override async Task PreProcessAsync(IPreProcessorContext context, AccountStateBag state, CancellationToken ct)
    {
        // Retrieve header
        var account = context.HttpContext.Request.Headers["X-Account-ID"];

        // Reject request if header missing
        if (String.IsNullOrEmpty(account))
        {
            await context.HttpContext.Response.SendStringAsync("Missing X-Account-ID header", 400, "text/plain");
        }
        else
        {
            // Otherwise place it into the "state bag" that is shared with the endpoint
            state.AccountId = account!;
        }
    }
}
