# Getting Started

This assessment project uses a .NET Core app. To start:

- Run `dotnet restore`
- Run `dotnet run`

For the assessment, the server is expected to be running and listening on port 5170. This is already
set in the default launchSettings.json.
