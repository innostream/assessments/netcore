import { useState } from "react";
import { deposit } from "./api";
import "./App.css";

function App() {
  const [amount, setAmount] = useState(500);
  const [balance, setBalance] = useState<number | undefined>(undefined);

  const performDeposit = async () => {
    try {
      const balance = await deposit(amount);
      setBalance(balance);
    } catch (err) {
      setBalance(undefined);
      alert(err);
    }
  };

  return (
    <>
      <h1>Account Deposit</h1>
      <div className="card">
        <form
          onSubmit={(e) => {
            performDeposit();
            e.preventDefault();
            return false;
          }}
        >
          <label>
            Amount:{" "}
            <input
              type="number"
              value={amount}
              onChange={(e) => setAmount(parseInt(e.target.value))}
              placeholder="Enter amount"
              min={1}
              max={10000}
            />
          </label>
          <input type="submit" value="Deposit" onClick={performDeposit} />
        </form>

        {balance ? <p>Your balance is ${balance.toFixed(2)}</p> : null}
      </div>
    </>
  );
}

export default App;
